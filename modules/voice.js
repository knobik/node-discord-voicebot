const BotModule = require('../utils/botModule');
const Tools = require('../utils/tools');

class Voice extends BotModule {

    constructor(application) {
        super(application);
    }

    ready() {
        this.initChannels();
    }

    initChannels() {

        this.assignVoiceRole();

        this.on('voiceStateUpdate', (oldMember, newMember) => {
            this.toggleVoiceRole(newMember);
        });

        this.on('guildMemberAdd', (member) => {
            let message = this.app.getConfig().welcome_message;
            if (message.length > 0) {
                member.send(message);

                console.log(`${member.user.username} joined the discord server.`);
            }
        });
    }

    assignVoiceRole() {
        this.app.getGuild().members.map((member) => {
            if (member.user.bot) {
                return;
            }

            this.toggleVoiceRole(member);
        });

    }

    toggleVoiceRole(member) {
        let voiceRole = this.app.getVoiceRole();

        if (member.voiceChannelID) {
            member.addRole(voiceRole);
            console.log(`${member.user.username} entered the voice channel.`);
        } else {
            member.removeRole(voiceRole);
            console.log(`${member.user.username} left the voice channel.`);
        }
    }

}

module.exports = Voice;