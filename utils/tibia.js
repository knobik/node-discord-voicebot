const request = require('request');
const cheerio = require('cheerio');

module.exports = {

    /**
     *
     * The returned value is a list with the lower limit and the upper limit in that order.
     *
     * @param level
     * @returns {{low: Number, high: Number}}
     */
    getShareRange(level) {
        return {
            low: parseInt(Math.round(level * 2 / 3)),
            high: parseInt(Math.round(level * 3 / 2))
        };
    },


    getCharacter(name, endCallback) {
        let url = `https://secure.tibia.com/community/?subtopic=characters&name=${encodeURIComponent(name)}`;
        request(url, (error, body, response) => {
            if (typeof response === 'undefined') {
                endCallback(null);
                return false;
            }

            let match,
                char = {
                    name: null,
                    deleted: false,
                    vocation: null,
                    level: null,
                    world: null,
                    residence: null,
                    married: null,
                    gender: null,
                    rank: null,
                    guild: null,
                    house_id: null,
                    house: null,
                    house_town: null,
                    last_login: null,
                    deaths: [],
                    chars: [],
                };

            // name
            if (match = response.match(/Name:<\/td><td>([^<,]+)/i)) {
                char.name = match[1]
            }

            // is deleted
            if (response.match(/, will be deleted at ([^<]+)/i)) {
                char.deleted = true;
            }

            // vocation
            if (match = response.match(/Vocation:<\/td><td>([^<]+)/i)) {
                char.vocation = match[1];
            }

            // level
            if (match = response.match(/Level:<\/td><td>(\d+)/i)) {
                char.level = parseInt(match[1]);
            }

            // world
            if (match = response.match(/World:<\/td><td>([^<]+)/i)) {
                char.world = match[1];
            }

            // home city
            if (match = response.match(/Residence:<\/td><td>([^<]+)/i)) {
                char.residence = match[1];
            }

            // relationship
            if (match = response.match(/Married To:<\/td><td>?.+name=([^"]+)/i)) {
                char.married = match[1];
            }

            // sex (✌ ﾟ ∀ ﾟ)☞
            if (match = response.match(/Sex:<\/td><td>([^<]+)/i)) {
                if (match[1] === 'male') {
                    char.gender = 'male';
                } else {
                    char.gender = 'female';
                }
            }

            // guild
            if (match = response.match(/Membership:<\/td><td>([^<]+)\sof the/i)) {
                char.rank = match[1];

                if (match = response.match(/GuildName=.*?([^&]+).+/i)) {
                    char.guild = match[1].split('+').join(' ');
                }
            }

            // house
            if (match = response.match(/House:<\/td><td> <a href="https:\/\/secure\.tibia\.com\/community\/\?subtopic=houses.+houseid=(\d+)&amp;character=(?:[^&]+)&amp;action=characters" >([^<]+)<\/a> \(([^(]+)\) is paid until ([A-z]+).*?;(\d+).*?;(\d+)/i)) {
                char.house_id = match[1];
                char.house = match[2];
                char.house_town = match[3];
            }

            // last login
            if (match = response.match(/Last Login:<\/td><td>([^<]+)/i)) {
                char.last_login = match[1].split("&#160;").join(" ").split(",").join("");
            }

            // deaths
            char.deaths = [];
            let re = /valign="top" >([^<]+)<\/td><td>(.+?)<\/td><\/tr>/ig;
            while (match = re.exec(response)) {
                let death_match = match[2],
                    death_time = match[1].replace(/&#160;/g, ' ').replace(/,/g, ''),
                    death_level = '',
                    death_killer = '',
                    death_by_player = false;

                if (death_match.indexOf('Died') > -1) {
                    if (match = death_match.match(/Level (\d+) by ([^.]+)/i)) {
                        death_level = match[1];
                        death_killer = match[2];
                    }
                } else {
                    if (match = death_match.match(/Level (\d+) by .+?name=([^"]+)/i)) {
                        death_level = match[1];
                        death_killer = match[2].replace(/\+/g, ' ');
                        death_by_player = true;
                    }
                }

                char.deaths.push({
                    time: death_time,
                    level: parseInt(death_level),
                    killer: death_killer,
                    byPlayer: death_by_player
                });
            }

            // other chars
            char.chars = [];
            if (response.indexOf('<B>Characters</B>') > -1) {

                let re = /<TD WIDTH=10%><NOBR>([^<]+)[^?]+[^]+?VALUE="([^"]+)/igm,
                    charContent = response.split('<B>Characters</B>')[1];

                while (match = re.exec(charContent)) {
                    char.chars.push({
                        name: match[2].replace(/\+/g, ' '),
                        world: match[1]
                    });
                }
            }

            endCallback(char);
            return true;
        });
    }
};