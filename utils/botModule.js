const Command = require('./command');

class BotModule {

    constructor(app) {
        this.app = app;
        this.lastChannel = null;
    }

    on(event, callback) {
        return this.app.getBot().on(event, callback);
    }

    addCommand(name, description, callback) {
        this.app.getParser().addCommand(new Command(this, name, description, callback));

        return this;
    }

    broadcast(text) {
        this.app.getLogChannels().forEach((channel) => {
            channel.send(text);
        });

        return this;
    }

    say(text) {
        if (!this.lastChannel) {
            throw new Error('Response channel is not set!');
        }

        this.lastChannel.send(text);

        return this;
    }
}

module.exports = BotModule;