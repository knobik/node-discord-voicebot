class CommandParser {

    constructor(app) {
        this.app = app;
        this.commands = [];
    }

    process(message) {
        let content = message.content;

        if (this.isAskChannel(message.channel.name) && this.isCommand(content)) {
            let parsed = this.splitCommand(content);

            this.commands.some((command) => { // bad hack, cuz we cant "break" forEach

                let commandNames = [command.name];
                if (command.name instanceof Array) {
                    commandNames = command.name;
                }

                if (commandNames.indexOf(parsed.command) > -1) {

                    command.module.lastChannel = message.channel;
                    command.callback(parsed.param, message, command.module);

                    return true;
                }
            });
        }
    }

    splitCommand(text) {
        let words = text.split(" "),
            command = words[0].substr(1);

        words.shift();

        let param = words.join(" ").trim();
        if (param === "") {
            param = null;
        }

        return {
            command: command.toLowerCase(),
            param: param
        };
    }

    isAskChannel(name) {
        return this.app.getConfig().ask_channels.indexOf(name) > -1;
    }

    isCommand(text) {
        return text.indexOf('/') == 0;
    }

    addCommand(command) {
        this.commands.push(command);
    }
}


module.exports = CommandParser;