class Command {
    constructor(module, name, description, callback) {
        this.module = module;
        this.name = name;
        this.description = description;
        this.callback = callback;
    }
}

module.exports = Command;