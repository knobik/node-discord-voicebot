const Discord = require('discord.js');
const CommandParser = require('./utils/parser');

class VoiceBot {

    constructor(config) {
        this.config = config;
        this.loadedModules = [];
        this.logChannels = [];
        this.cache = {};

        this.discord = new Discord.Client();
        this.commandParser = new CommandParser(this);

        this.bindDiscordEvents();
        this.loadModules();

        this.getBot().login(config.auth.token);
    }

    loadModules() {
        this.getConfig().modules.forEach((module) => {
            this.loadedModules.push(new (require("./modules/" + module))(this));
        });

        return this;
    }

    bindDiscordEvents() {
        this.getBot().on('ready', this.onReady);
        this.getBot().on('message', this.onMessage);

        return this;
    }

    onReady() {
        Bot.loadLogChannels();

        Bot.loadedModules.forEach((module) => {
            if (typeof module.ready !== 'undefined') {
                module.ready();
            }
        });

        console.log('Im ready!');
    }

    onMessage(message) {
        if (Bot.getConfig().ask_channels.indexOf(message.channel.name) > -1) {
            Bot.commandParser.process(message);
        }
    }

    loadLogChannels() {
        this.logChannels = this.getGuild().channels.filter((channel) => {
            return Bot.getConfig().log_channels.indexOf(channel.name) > -1;
        }).array();
    }

    getConfig() {
        return this.config;
    }

    getParser() {
        return this.commandParser;
    }

    getBot() {
        return this.discord;
    }

    getGuild() {
        return this.getBot().guilds.get(this.getConfig().guild_id);
    }

    getGeneralTextChannel() {
        if (typeof this.cache.generalTextChannel !== 'undefined') {
            return this.cache.generalTextChannel;
        }

        this.cache.generalTextChannel = this.getGuild().channels.find((channel) => {
            return channel.constructor.name === 'TextChannel' && channel.name.toLowerCase() === 'general';
        });

        return this.cache.generalTextChannel;
    }

    getVoiceRole() {
        if (typeof this.cache.voiceRole !== 'undefined') {
            return this.cache.voiceRole;
        }

        this.cache.voiceRole = this.getGuild().roles.find('name', this.getConfig().voice_role_name);

        return this.cache.voiceRole;
    }

    getLogChannels() {
        return this.logChannels;
    }
}

// ---- main
const Bot = new VoiceBot(require('./config'));
